<?php

class VP_Control_Field_Map extends VP_Control_Field
{


    private $lat;
    private $lng;
    private $address;

    public function __construct()
    {
        parent::__construct();
    }

    public static function withArray($arr = array(), $class_name = null)
    {
        if (is_null($class_name)) {
            $instance = new self();
        } else {
            $instance = new $class_name;
        }
        $instance->_basic_make($arr);

        return $instance;
    }

    protected function set_lat($lat)
    {

        $this->lat = $lat;

        return $this;
    }

    protected function set_lng($lng)
    {

        $this->lng = $lng;

        return $this;
    }

    protected function set_address($address)
    {

        $this->address = $address;

        return $this;
    }

    protected function get_lat()
    {
        return $this->lat;
    }

    protected function get_lng()
    {
        return $this->lng;
    }

    protected function get_address()
    {
        return $this->address;
    }

    public function render($is_compact = false)
    {
        // Setup Data
        $this->_setup_data();
        $this->add_data('is_compact', $is_compact);

        return VP_View::instance()->load('control/map', $this->get_data());
    }

}

/**
 * EOF
 */