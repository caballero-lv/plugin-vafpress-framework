<?php
////////////////////////////////////////////////
// Scripts and Styles Dependencies Definition //
////////////////////////////////////////////////


/**
 * jQuery UI Theme
 */
$jqui_theme = 'smoothness';
$dependencies['jqui_theme'] = $jqui_theme;

/**
 * Scripts.
 */

$dependencies['scripts'] = array();

$dependencies['scripts']['always'] = array('jquery', 'scrollspy', 'tipsy', 'jquery-typing');

$dependencies['scripts']['paths'] = array();
$dependencies['scripts']['paths']['jquery'] = array(
    'path' => '',
    'deps' => array(),
    'ver' => '1.8.3',
    'override' => false,
);
$dependencies['scripts']['paths']['bootstrap-colorpicker'] = array(
    'path' => VP_PUBLIC_URL . '/js/vendor/bootstrap-colorpicker.js',
    'deps' => array('jquery'),
    'ver' => false,
);
$dependencies['scripts']['paths']['tipsy'] = array(
    'path' => VP_PUBLIC_URL . '/js/vendor/jquery.tipsy.js',
    'deps' => array('jquery'),
    'ver' => '1.0.0a'
);
$dependencies['scripts']['paths']['scrollspy'] = array(
    'path' => VP_PUBLIC_URL . '/js/vendor/jquery-scrollspy.js',
    'deps' => array('jquery'),
    'ver' => false,
);
$dependencies['scripts']['paths']['jquery-ui-core'] = array(
    'path' => '',
    'deps' => array(),
    'ver' => '1.9.2',
);
$dependencies['scripts']['paths']['jquery-ui-widget'] = array(
    'path' => '',
    'deps' => array(),
    'ver' => '1.9.2',
);
$dependencies['scripts']['paths']['jquery-ui-mouse'] = array(
    'path' => '',
    'deps' => array('jquery-ui-widget'),
    'ver' => '1.9.2',
);
$dependencies['scripts']['paths']['jquery-ui-slider'] = array(
    'path' => '',
    'deps' => array('jquery', 'jquery-ui-core', 'jquery-ui-widget', 'jquery-ui-mouse'),
    'ver' => '1.9.2',
);
$dependencies['scripts']['paths']['jquery-ui-datepicker'] = array(
    'path' => '',
    'deps' => array('jquery', 'jquery-ui-core', 'jquery-ui-widget'),
    'ver' => '1.9.2',
);

$dependencies['scripts']['paths']['gmaps'] = array(
    'path' => '//maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBaob1XUk6NQ7RmmsB4QuhqjGh3foYE3EE',
    'deps' => array(),
    'ver' => '3.0',
);
$dependencies['scripts']['paths']['df-post-gallery'] = array(
    'path' => VP_PUBLIC_URL . '/js/vendor/df-post-gallery.js',
    'deps' => array(),
    'ver' => '1.0',
);

$dependencies['scripts']['paths']['jquery-typing'] = array(
    'path' => VP_PUBLIC_URL . '/js/vendor/jquery.typing-0.2.0.min.js',
    'deps' => array('jquery'),
    'ver' => '0.2',
);
$dependencies['scripts']['paths']['ace-editor'] = array(
    'path' => VP_PUBLIC_URL . '/js/vendor/ace/ace.js',
    'deps' => array(),
    'ver' => '1.0.0',
);
$dependencies['scripts']['paths']['select2'] = array(
    'path' => VP_PUBLIC_URL . '/js/vendor/select2.min.js',
    'deps' => array('jquery'),
    'ver' => '3.3.2',
    'override' => true,
);
$dependencies['scripts']['paths']['select2-sortable'] = array(
    'path' => VP_PUBLIC_URL . '/js/vendor/select2.sortable.js',
    'deps' => array('jquery', 'jquery-ui-sortable', 'select2'),
    'ver' => '1.0.0',
    'override' => true,
);
$dependencies['scripts']['paths']['reveal'] = array(
    'path' => VP_PUBLIC_URL . '/js/vendor/jquery.reveal.js',
    'deps' => array('jquery'),
    'ver' => '1.0.0',
);
$dependencies['scripts']['paths']['kia-metabox'] = array(
    'path' => VP_PUBLIC_URL . '/js/kia-metabox.js',
    'deps' => array('jquery', 'editor'),
    'ver' => '1.0',
    'override' => true,
);
$dependencies['scripts']['paths']['shared'] = array(
    'path' => VP_PUBLIC_URL . '/js/shared.min.js',
    'deps' => array(),
    'ver' => '1.1',
    'localize' => array(
        'name' => 'vp_wp',
        'keys' => array(
            'use_upload',
            'use_new_media_upload',
            'public_url',
            'wp_include_url',
            'val_msg',
            'ctrl_msg',
            'alphabet_validatable',
            'alphanumeric_validatable',
            'numeric_validatable',
            'email_validatable',
            'url_validatable',
            'maxlength_validatable',
            'minlength_validatable'
        )
    )
);
$dependencies['scripts']['paths']['vp-option'] = array(
    'path' => VP_PUBLIC_URL . '/js/option.min.js',
    'deps' => array(),
    'ver' => '2.0',
    'localize' => array(
        'name' => 'vp_opt',
        'keys' => array(
            'util_msg',
            'nonce'
        )
    )
);
$dependencies['scripts']['paths']['vp-metabox'] = array(
    'path' => VP_PUBLIC_URL . '/js/metabox.min.js',
    'deps' => array(),
    'ver' => '2.0',
    'localize' => array(
        'name' => 'vp_mb',
        'keys' => array(
            'use_upload',
            'use_new_media_upload'
        )
    )
);
$dependencies['scripts']['paths']['vp-shortcode-qt'] = array(
    'path' => VP_PUBLIC_URL . '/js/shortcode-quicktags.js',
    'deps' => array('reveal'),
    'ver' => '1.0.0',
);
$dependencies['scripts']['paths']['vp-shortcode'] = array(
    'path' => VP_PUBLIC_URL . '/js/shortcode-menu.js',
    'deps' => array('reveal', 'vp-shortcode-qt'),
    'ver' => '2.0',
    'localize' => array(
        'name' => 'vp_ext_sc',
        'keys' => array(
            'use_upload',
            'use_new_media_upload',
            'public_url'
        )
    )
);

/**
 * Styles.
 */
$dependencies['styles'] = array(
    'always' => array('tipsy', 'font-awesome'),
    'paths' => array(
        'bootstrap-colorpicker' => array(
            'path' => VP_PUBLIC_URL . '/css/vendor/bootstrap-colorpicker.css',
            'deps' => array(),
        ),
        'tipsy' => array(
            'path' => VP_PUBLIC_URL . '/css/vendor/tipsy.css',
            'deps' => array(),
        ),
        'jqui' => array(
            'path' => VP_PUBLIC_URL . '/css/vendor/jqueryui/themes/' . $jqui_theme . '/jquery-ui-1.9.2.custom.min.css',
            'deps' => array(),
        ),
        'font-awesome' => array(
            'path' => VP_PUBLIC_URL . '/css/vendor/font-awesome.min.css',
            'deps' => array(),
        ),
        'select2' => array(
            'path' => VP_PUBLIC_URL . '/css/vendor/select2.css',
            'deps' => array(),
        ),
        'reveal' => array(
            'path' => VP_PUBLIC_URL . '/css/vendor/reveal.css',
            'deps' => array(),
        ),
        'vp-option' => array(
            'path' => VP_PUBLIC_URL . '/css/option.min.css',
            'deps' => array(),
        ),
        'vp-metabox' => array(
            'path' => VP_PUBLIC_URL . '/css/metabox.min.css',
            'deps' => array(),
        ),
        'vp-shortcode' => array(
            'path' => VP_PUBLIC_URL . '/css/shortcode.min.css',
            'deps' => array('reveal'),
        ),
        'df-post-gallery' => array(
            'path' => VP_PUBLIC_URL . '/css/vendor/df-post-gallery.css',
            'deps' => array(),
        ),
    )
);

/**
 * Rules for dynamic loading of dependencies, load only what needed.
 */
$dependencies['rules'] = array(
    'color' => array('js' => array('bootstrap-colorpicker'), 'css' => array('bootstrap-colorpicker')),
    'gallery' => array('js' => array('df-post-gallery'), 'css' => array('df-post-gallery')),
    'select' => array('js' => array('select2'), 'css' => array('select2')),
    'multiselect' => array('js' => array('select2'), 'css' => array('select2')),
    'slider' => array('js' => array('jquery-ui-slider'), 'css' => array('jqui')),
    'date' => array('js' => array('jquery-ui-datepicker'), 'css' => array('jqui')),
    'codeeditor' => array('js' => array('ace-editor'), 'css' => array()),
    'sorter' => array('js' => array('select2-sortable'), 'css' => array('select2', 'jqui')),
    'fontawesome' => array('js' => array('select2'), 'css' => array('select2')),
    'wpeditor' => array('js' => array('kia-metabox'), 'css' => array()),
    'map' => array('js' => array('gmaps'), 'css' => array()),
);

return $dependencies;
/**
 * EOF
 */