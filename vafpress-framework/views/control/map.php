<style>
    .gmap-input {
        display: block;
        width: 100%;
        margin: 0;
        font-size: 12px;
        line-height: 16px;
        padding: 10px;
        border: 0 none;
        font-family: inherit;
        cursor: text;
        outline: none;
    }

    .gmap img {
        max-width: none;
        width: auto;
    }
</style>
<?php if (!$is_compact) {
    echo VP_View::instance()->load('control/template_control_head', $head_info);
} ?>
<input type="hidden" value='<?php echo $value; ?>' name="<?php echo $name; ?>" id="<?php echo $name; ?>-output"/>

<?php
$address = '';
if (isset($data['address'])) {
    $address = $data['address'];
}
?>

<input class="gmap-input" type="text" id="<?php echo $name; ?>-input" value="<?php echo $address; ?>"/>
<div id="<?php echo $name; ?>-map" style="height:350px;" class="gmap"></div>
<?php if (!$is_compact) {
    echo VP_View::instance()->load('control/template_control_foot');
} ?>