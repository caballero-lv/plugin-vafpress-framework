<?php if (!$is_compact) {
    echo VP_View::instance()->load('control/template_control_head', $head_info);
} ?>

<?php

$editor_settings = array(
    'teeny' => true,
    'wpautop' => true,
    'media_buttons' => true,
    'textarea_name' => $name
);

/**
 * Remove [, ], {, } (brackets), - (hypen) from Editor ID
 * @since 3.9
 * TinyMCE editor IDs cannot have brackets.
 */

$id = str_replace(array("[", "{", "-", "]", "}"), "Q", $name);
$id = $id . '_ce';

?>

<div class="customEditor">
    <?php wp_editor($value, $id, $editor_settings); ?>
</div>
<?php if (!$is_compact) {
    echo VP_View::instance()->load('control/template_control_foot');
} ?>
