<?php if (!$is_compact) {
    echo VP_View::instance()->load('control/template_control_head', $head_info);
} ?>

    <div class="caballero-images">
        <div class="inside">
            <div class="product_images_container">
                <ul class="product_images clearfix">

                    <?php $attachments = array_filter(explode(',', $value));

                    if ($attachments) {
                        foreach ($attachments as $attachment_id) {
                            echo '<li class="image" data-attachment_id="' . esc_attr($attachment_id) . '">
									' . wp_get_attachment_image($attachment_id, 'thumbnail') . '
									<ul class="actions">
										<li><a href="#" class="delete tips" data-tip="' . __('Delete image',
                                    'vp_textdomain') . '">' . __('Delete', 'vp_textdomain') . '</a></li>
									</ul>
								</li>';
                        }
                    }
                    ?>
                </ul>

                <a class="add_product_images button hide-if-no-js" href="#"
                   data-choose="<?php echo __('Add Images to Gallery', 'vp_textdomain'); ?>"
                   data-update="<?php echo __('Add to gallery', 'vp_textdomain'); ?>"
                   data-delete="<?php echo __('Delete image', 'vp_textdomain'); ?>"
                   data-text="<?php _e('Delete', 'vp_textdomain'); ?>"><?php echo __('Add gallery images',
                        'vp_textdomain'); ?></a>

                <input type="hidden" class="product_image_gallery" name="<?php echo $name ?>"
                       class="vp-input input-large"
                       value="<?php echo esc_attr($value); ?>"/>

            </div>
        </div>
    </div>

<?php if (!$is_compact) {
    echo VP_View::instance()->load('control/template_control_foot');
} ?>