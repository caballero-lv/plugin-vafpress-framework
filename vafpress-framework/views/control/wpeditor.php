<?php if (!$is_compact) {
    echo VP_View::instance()->load('control/template_control_head', $head_info);
} ?>

<?php

$editor_settings = array(
    'wpautop' => true,
    'media_buttons' => false,
    'textarea_name' => $name
);

/**
 * Remove [, ], {, } (brackets), - (hypen) from Editor ID
 * @since 3.9
 * TinyMCE editor IDs cannot have brackets.
 */

$id = str_replace(array("[", "{", "-", "]", "}"), "Q", $name);
$id = $id . '_ce';

?>

<div class="customEditor">
    <div class="wp-editor-tools">
        <div class="custom_upload_buttons hide-if-no-js wp-media-buttons"><?php do_action('media_buttons'); ?></div>
    </div>
    <div class="wp-editor-wrapper">
        <?php wp_editor($value, $id, $editor_settings); ?>
    </div>
</div>
<?php if (!$is_compact) {
    echo VP_View::instance()->load('control/template_control_foot');
} ?>
