<?php if (!$is_compact) {
    echo VP_View::instance()->load('control/template_control_head', $head_info);
} ?>

<?php $post_types = get_post_types(array(
    'public' => true
), 'objects'); ?>

    <label class="vp-field">
        <input class="vp-input" <?php if (in_array("all", $value)) {
            echo " checked=\"checked\"";
        } ?>" type="checkbox" name="<?php echo $name; ?>" value="all"/>
        <span><?php echo __('Show on all pages'); ?></span>
    </label>

<?php foreach ($post_types as $item): ?>
    <label class="vp-field">
        <?php
        $s_query = new WP_Query(array(
            'suppress_filters' => false,
            'post_type' => $item->name,
            'post_status' => 'publish'
        ));
        if ($s_query->have_posts()) { ?>
            <h2><?php echo ucwords($item->name); ?></h2>
            <select multiple class="vp-input vp-js-select2" type="select" autocomplete="off" name="<?php echo $name; ?>">
                <?php while ($s_query->have_posts()): $s_query->the_post(); ?>
                    <option <?php if(in_array(get_the_ID(), $value)) echo "selected" ?> value="<?php the_ID(); ?>"><?php the_title(); ?></option>
                    <?php
                endwhile;
                wp_reset_query(); ?>
            </select>
        <?php } ?>
    </label>
<?php endforeach; ?>

<?php if (!$is_compact) {
    echo VP_View::instance()->load('control/template_control_foot');
} ?>